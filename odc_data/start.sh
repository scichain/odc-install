#!/bin/bash
docker_username="opendatachain@1700029786238904"
docker_password="opendatachain123"
docker_version=""
docker_registry="registry.cn-beijing.aliyuncs.com"

sudo echo -e "\033[32m --start-- \033[0m"
sudo echo -e "\033[33m please input version: \033[0m"

read -r docker_version

sudo echo -e "\033[33m docker_version:${docker_version} \033[0m"

sudo docker login --username=$docker_username --password=$docker_password ${docker_registry}

sudo echo -e "\033[32m 1.pull image from route \033[0m"
sudo docker pull "$docker_registry/opendatachain/openjdk:11-jre-slim"
sudo docker pull "$docker_registry/opendatachain/mongo:5.0.10"
sudo docker pull "$docker_registry/opendatachain/kibana:7.9.3"
sudo docker pull "$docker_registry/opendatachain/elasticsearch:7.9.3"
sudo docker pull "$docker_registry/opendatachain/rabbitmq:3.8.3-management"
sudo docker pull "$docker_registry/opendatachain/odc-node:$docker_version"
sudo docker pull "$docker_registry/opendatachain/odc-order:$docker_version"

sudo echo -e "\033[32m 2.tag images \033[0m"
sudo docker tag "$docker_registry/opendatachain/openjdk:11-jre-slim" openjdk:11-jre-slim
sudo docker tag "$docker_registry/opendatachain/mongo:latest" mongo:5.0.10
sudo docker tag "$docker_registry/opendatachain/kibana:7.9.3" kibana:7.9.3
sudo docker tag "$docker_registry/opendatachain/elasticsearch:7.9.3" elasticsearch:7.9.3
sudo docker tag "$docker_registry/opendatachain/rabbitmq:3.8.3-management" rabbitmq:3.8.3-management
sudo docker tag "$docker_registry/opendatachain/odc-node:$docker_version" odc-node:$docker_version
sudo docker tag "$docker_registry/opendatachain/odc-order:$docker_version" odc-order:$docker_version

sudo echo -e "\033[32m 3.remove old tag images \033[0m"
sudo docker rmi "$docker_registry/opendatachain/openjdk:11-jre-slim"
sudo docker rmi "$docker_registry/opendatachain/mongo:5.0.10"
sudo docker rmi "$docker_registry/opendatachain/kibana:7.9.3"
sudo docker rmi "$docker_registry/opendatachain/elasticsearch:7.9.3"
sudo docker rmi "$docker_registry/opendatachain/rabbitmq:3.8.3-management"
sudo docker rmi "$docker_registry/opendatachain/odc-node:$docker_version"
sudo docker rmi "$docker_registry/opendatachain/odc-order:$docker_version"

sudo docker images

sudo echo -e "\033[32m 4.running docker-compose  \033[0m"
docker-compose -f odc-base-compose.yaml up -d

sudo echo -e "\033[32m --wait base container build... --  \033[0m"
sleep 5s

docker-compose -f odc-node-compose.yaml up -d

sudo echo -e "\033[32m --wait node container build... --  \033[0m"
sleep 5s

docker-compose -f odc-order-compose.yaml up -d

sudo echo -e "\033[32m --wait order container build... --  \033[0m"
sleep 3s

sudo docker ps 

sudo echo -e "\033[32m --end-- \033[0m"

 

